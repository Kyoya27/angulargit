const pokemon_file = require('./../dist/pokemon');
const fight_file = require('./../dist/fight');
const attack_file = require('./../dist/attack');


describe('First test', () => {
    test('Get pokemon name', () => {
        let pokemon = new pokemon_file.Pokemon("Pikachu");
        expect(pokemon.name).toBe("Pikachu");
    });
});

describe('Priority Tests', () => {

    test('First speed', () => {
        let pokemon = new pokemon_file.Pokemon("Pikachu", 20, 20, 15, 15, 20, 20, [], "FIRE", 10);
        expect(pokemon.get_speed()).toBe(20);
    });

    test('With status speed', () => {
        let pokemon = new pokemon_file.Pokemon("Pikachu", 20, 20, 15, 15, 20, 20, [], "FIRE", 10);
        pokemon.status = "Ill";
        expect(pokemon.get_speed()).toBe(10);
    });
});


describe('Fight Tests', () => {


        test('Elemental stage effect', () => {
            let fight = new fight_file.Fight(new pokemon_file.Pokemon("Pikachu", 20, 20, 15, 15, 20, 20, [], "FIRE", 10), new pokemon_file.Pokemon("Raichu", 20, 20, 15, 15, 20, 20, [], "WATER", 10));
            fight.stage = "WATER";
            let winner = fight.attack_order()
            expect(winner.name).toBe("Pikachu");
        });



        test('Only Speed Battle', () => {
            let fight = new fight_file.Fight(new pokemon_file.Pokemon("Pikachu", 20, 20, 15, 15, 20, 20, [], "FIRE", 10), new pokemon_file.Pokemon("Raichu", 40, 20, 15, 15, 20, 20, [], "FIRE", 10));
            let winner = fight.attack_order()
            expect(winner.name).toBe("Raichu");
        });

        test('Only BUT Speed Battle', () => {

            let fight = new fight_file.Fight(new pokemon_file.Pokemon("Pikachu", 20, 20, 15, 15, 20, 20, [], "FIRE", 10), new pokemon_file.Pokemon("Raichu", 20, 50, 50, 50, 60, 60, [], "WATER", 10));
            let winner = fight.attack_order()
            expect(winner.name).toBe("Pikachu");
        });




        test('Speed Battle Damage', () => {
            let Lightning = new attack_file.Attack("Lightning", 0.25, "NORMAL", "Bolt effect", "NO", 0, 100, true, true);
            let fight = new fight_file.Fight(new pokemon_file.Pokemon("Pikachu", 20, 50, 50, 50, 60, 60, [Lightning], "FIRE", 10), new pokemon_file.Pokemon("Raichu", 40, 20, 15, 15, 20, 20, [Lightning], "FIRE", 10));
            let damage = fight.attack_damage(fight.p1, fight.p2);
            expect(damage).toBe(0);
        });

        test('Spe Battle Damage', () => {
            let Lightning = new attack_file.Attack("Lightning", 0.25, "NORMAL", "Bolt effect", "NO", 0, 100, true, true);
            let fight = new fight_file.Fight(new pokemon_file.Pokemon("Pikachu", 20, 50, 50, 50, 100, 100, [Lightning], "FIRE", 10), new pokemon_file.Pokemon("Raichu", 20, 50, 50, 50, 60, 60, [Lightning], "FIRE", 10));
            let damage = fight.attack_damage(fight.p1, fight.p2);
            expect(damage).toBe(0);
        });



        test('Hit', () => {
            let Lightning = new attack_file.Attack("Lightning", 0.25, "NORMAL", "Bolt effect", "NO", 0, 100, false, false);
            let Dekopin = new attack_file.Attack("Psycho", 2, "PSY", "Itai itai", "NO", 75, 100, false, false);

            let fight = new fight_file.Fight(new pokemon_file.Pokemon("Pikachu", 20, 50, 50, 50, 100, 100, [Lightning], "FIRE", 10), new pokemon_file.Pokemon("Raichu", 20, 50, 50, 50, 60, 60, [Lightning], "FIRE", 10));
            let hit = fight.hit(Lightning, fight.p2, 0.2);
            expect(hit).toBe(true);
        });

        test('Luck evade', () => {
            let Lightning = new attack_file.Attack("Lightning", 0.25, "NORMAL", "Bolt effect", "NO", 0, 100, false, false);
            let Dekopin = new attack_file.Attack("Psycho", 2, "PSY", "Itai itai", "NO", 75, 100, false, false);

            let fight = new fight_file.Fight(new pokemon_file.Pokemon("Pikachu", 20, 50, 50, 50, 100, 100, [Lightning], "FIRE", 10), new pokemon_file.Pokemon("Raichu", 20, 50, 50, 50, 60, 60, [Lightning], "FIRE", 10));
            let hit = fight.hit(Lightning, fight.p1, 0.2);
            expect(hit).toBe(true);
        });

        test('Not hit', () => {
            let Lightning = new attack_file.Attack("Lightning", 0.25, "NORMAL", "Bolt effect", "NO", 0, 100, false, false);
            let Dekopin = new attack_file.Attack("Psycho", 2, "PSY", "Itai itai", "NO", 75, 100, false, false);


            let fight = new fight_file.Fight(new pokemon_file.Pokemon("Pikachu", 20, 50, 50, 50, 100, 100, [Lightning], "FIRE", 10), new pokemon_file.Pokemon("Raichu", 20, 50, 50, 50, 60, 60, [Lightning], "FIRE", 10));
            let hit = fight.hit(Lightning, fight.p1, 0.8);
            expect(hit).toBe(true);
        });


});
