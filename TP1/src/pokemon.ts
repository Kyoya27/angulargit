import { Attack } from "./attack";
import { Item } from "./item";


export class Pokemon {
    name: string;

	speed: number;
	status: string;
	element: string;

	start_life_point: number;
	life_point: number;

	attack: number;
	sp_attack: number;
	defense: number;
	sp_defense: number;
	attacks: Array<Attack>;

	luck: number;

    constructor(_name: string,
			     _speed: number,
			     _life_point: number,
			     _attack: number,
			     _sp_attack: number,
			     _defense: number,
			     _sp_defense: number,
			     _attacks: Array<Attack>,
			     _element: string, 
			     _luck: number) {

    	this.name = _name;

    	this.speed = _speed;
    	this.element = _element
    	this.status = "null";

    	this.start_life_point = _life_point;
    	this.life_point = _life_point;

    	this.attack = _attack;
    	this.sp_attack = _sp_attack;
    	this.defense = _defense;
    	this.sp_defense = _sp_defense;
    	this.attacks = _attacks;

    	this.luck = _luck;
    }

    get_speed(){
    	return (this.status === "Ill" ? this.speed/2 : this.speed);
    }

}