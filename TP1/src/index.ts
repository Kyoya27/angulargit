import { Attack } from "./attack";
import { Pokemon } from "./pokemon";
import { Fight } from "./fight";

let Lightning = new Attack("Lightning", 0.25, "NORMAL", "Bolt effect", "NO", 0, 100, false, false);
let Dekopin = new Attack("Psycho", 2, "PSY", "Itai itai", "NO", 75, 100, false, false);

let pikachu = new Pokemon("Pikachu", 20, 50, 50, 50, 60, 60, [Lightning], "FIRE", 10);
let raichu = new Pokemon("Raichu", 40, 20, 15, 15, 20, 20, [Lightning], "FIRE", 10);

let fight = new Fight(pikachu, raichu);
