export class Attack {

	name: string;
	ratio: number;
    element: string;
    description: string;
    status: string;
    status_ratio: number;
    precision: number;
    special: boolean;
    priority: boolean;

    constructor(name: string, ratio: number, element: string, description: string, status: string, status_ratio: number, precision: number, _special: boolean, priority: boolean) {
    	this.name = name;
        this.ratio = ratio;
        this.element = element;
        this.description = description;
        this.status = status;
        this.status_ratio = status_ratio;
        this.special = _special;
        this.precision = precision;
        this.priority = priority;
    }

}