"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Fight = /** @class */ (function () {
    function Fight(_p1, _p2) {
        this.elements = ["FIRE", "WATER", "GRASS", "PSY", "NORMAL"];
        this.type_ratio = [[0.5, 0.25, 2, 1, 1],
            [2, 0.5, 0.25, 1, 1],
            [0.25, 2, 0.5, 1, 1],
            [1, 1, 1, 0.5, 2],
            [1, 1, 1, 1, 0.5]];
        this.game = false;
        this.p1 = _p1;
        this.p2 = _p2;
        this.stage_element = "FIRE";
    }
    Fight.prototype.attack_order = function () {
        var p1_speed = this.p1.get_speed();
        var p2_speed = this.p2.get_speed();
        this.p1.element === this.stage_element ? p1_speed *= 2 : p1_speed = p1_speed;
        this.p2.element === this.stage_element ? p2_speed *= 2 : p2_speed = p2_speed;
        if (p1_speed > p2_speed)
            return this.p1;
        return this.p2;
    };
    Fight.prototype.attack_damage = function (p1, p2) {
        var damage = 0;
        if (p1.attacks[0].special) {
            damage += (p1.attack) * p1.attacks[0].ratio;
            if (p1.element === p1.attacks[0].element) {
                damage *= 2;
            }
            damage *= this.type_ratio[this.elements.indexOf(p1.attacks[0].element)][this.elements.indexOf(p2.element)];
            damage = damage - p2.defense;
        }
        else {
            damage += (p1.sp_attack) * p1.attacks[0].ratio;
            if (p1.element === p1.attacks[0].element) {
                damage *= 2;
            }
            damage *= this.type_ratio[this.elements.indexOf(p1.attacks[0].element)][this.elements.indexOf(p2.element)];
            damage = damage - p2.sp_defense;
        }
        return damage < 0 ? 0 : damage;
    };
    Fight.prototype.hit = function (attack, p2, proba) {
        var precision = attack.precision - p2.luck;
        return precision > proba * 100 ? true : false;
    };
    Fight.prototype.encounter = function () {
        if (this.p1 === this.attack_order()) {
            console.log(this.p1.name + " prepare is attack");
            var random = Math.random();
            if (this.hit(this.p1.attacks[0], this.p2, random)) {
                var damage = this.attack_damage(this.p1, this.p2);
                this.p2.life_point -= damage;
                console.log(this.p1.name + " attacks with " + this.p1.attacks[0].name);
                console.log(this.p2.name + " takes " + damage);
                console.log(this.p2.name + " hp " + this.p2.life_point + "/" + this.p2.start_life_point);
            }
            if (this.captured()) {
                return true;
            }
            random = Math.random();
            if (this.hit(this.p2.attacks[0], this.p1, random)) {
                var damage = this.attack_damage(this.p2, this.p1);
                this.p1.life_point -= damage;
                console.log(this.p2.name + " attacks with " + this.p2.attacks[0].name);
                console.log(this.p1.name + " takes " + damage);
                console.log(this.p1.name + " hp " + this.p1.life_point + "/" + this.p1.start_life_point);
            }
            if (this.captured())
                return true;
        }
        else {
            var random = Math.random();
            if (this.hit(this.p2.attacks[0], this.p1, random)) {
                var damage = this.attack_damage(this.p2, this.p1);
                this.p1.life_point -= damage;
                console.log(this.p2.name + " attacks with " + this.p2.attacks[0].name);
                console.log(this.p1.name + " takes " + damage);
                console.log(this.p1.name + " life points :  " + this.p1.life_point + "/" + this.p1.start_life_point);
            }
            if (this.captured()) {
                return true;
            }
            random = Math.random();
            if (this.hit(this.p1.attacks[0], this.p2, random)) {
                var damage = this.attack_damage(this.p1, this.p2);
                this.p2.life_point -= damage;
                console.log(this.p1.name + " attacks with " + this.p1.attacks[0].name);
                console.log(this.p2.name + " takes " + damage);
                console.log(this.p2.name + " life points : " + this.p2.life_point + "/" + this.p2.start_life_point);
            }
            if (this.captured())
                return true;
        }
        return false;
    };
    Fight.prototype.captured = function () {
        if (this.p1.life_point >= 1 && this.p2.life_point >= 1)
            return false;
        if (this.p1.life_point <= 0) {
            console.log(this.p1.name + " captured");
            this.game = true;
            return true;
        }
        if (this.p2.life_point <= 0) {
            console.log(this.p2.name + "captured");
            this.game = true;
            return true;
        }
    };
    return Fight;
}());
exports.Fight = Fight;
//# sourceMappingURL=fight.js.map