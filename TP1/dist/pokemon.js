"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Pokemon = /** @class */ (function () {
    function Pokemon(_name, _speed, _life_point, _attack, _sp_attack, _defense, _sp_defense, _attacks, _element, _luck) {
        this.name = _name;
        this.speed = _speed;
        this.element = _element;
        this.status = "null";
        this.start_life_point = _life_point;
        this.life_point = _life_point;
        this.attack = _attack;
        this.sp_attack = _sp_attack;
        this.defense = _defense;
        this.sp_defense = _sp_defense;
        this.attacks = _attacks;
        this.luck = _luck;
    }
    Pokemon.prototype.get_speed = function () {
        return (this.status === "Ill" ? this.speed / 2 : this.speed);
    };
    return Pokemon;
}());
exports.Pokemon = Pokemon;
//# sourceMappingURL=pokemon.js.map