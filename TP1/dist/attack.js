"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Attack = /** @class */ (function () {
    function Attack(name, ratio, element, description, status, status_ratio, precision, _special, priority) {
        this.name = name;
        this.ratio = ratio;
        this.element = element;
        this.description = description;
        this.status = status;
        this.status_ratio = status_ratio;
        this.special = _special;
        this.precision = precision;
        this.priority = priority;
    }
    return Attack;
}());
exports.Attack = Attack;
//# sourceMappingURL=attack.js.map