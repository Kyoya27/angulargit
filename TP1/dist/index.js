"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var attack_1 = require("./attack");
var pokemon_1 = require("./pokemon");
var fight_1 = require("./fight");
var Lightning = new attack_1.Attack("Lightning", 0.25, "NORMAL", "Bolt effect", "NO", 0, 100, false, false);
var Dekopin = new attack_1.Attack("Psycho", 2, "PSY", "Itai itai", "NO", 75, 100, false, false);
var pikachu = new pokemon_1.Pokemon("Pikachu", 20, 50, 50, 50, 60, 60, [Lightning], "FIRE", 10);
var raichu = new pokemon_1.Pokemon("Raichu", 40, 20, 15, 15, 20, 20, [Lightning], "FIRE", 10);
var fight = new fight_1.Fight(pikachu, raichu);
//# sourceMappingURL=index.js.map